# Java Examples

This is a set of sample programs demonstrating various features of the Java language.

## Java Fundamentals

1. `Example1`: displays a multiplication table to one hundred using `String.format()` function for formatting.
1. `Example2`: counts the number of occurrences of words in the text and writes them to the console in alphabetical order.
   This program uses two methods: `Arrays.sort()` and `Stream`.

## Java Fundamentals - coding

1. `Example1`: shows addition of two binary strings.

## Java Fundamentals - Advanced Features

1. `Example1`: demonstration of a local class.
1. `Example2`: demonstration of anonymous classes.
1. `Example5`: demonstrates the use of lambda expressions, generics and aggregate operations.

## Software Testing

1. `Junit/ExampleA`: shows the use of the reference `Test`.
1. `Assertj/ExampleA`.

## Design Patterns and Best Practices

### Structural patterns

1. `adapter`.
1. `bridge`.
1. `composite`.
1. `decorator`.
1. `facade`.
1. `flyweight`.
1. `proxy`.

### Behavioral Patterns

1. `chain_of_responsibility`.
1. `command`.
1. `interpreter`.
1. `iterator`.
1. `mediator`.
1. `memento`.
1. `observer`.
1. `state`.
1. `strategy`.
1. `template_method`.
1. `visitor`.

## Spring
