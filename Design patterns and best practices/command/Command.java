public interface Command {
   void apply();
   void cancel();
}
