public interface FragStatistics {
   int incrementFragCount();
   int incrementDeathCount();
   void reset();
}
