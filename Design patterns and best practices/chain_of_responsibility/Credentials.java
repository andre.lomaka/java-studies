public interface Credentials {
   String getCredentials(String userId);
}
