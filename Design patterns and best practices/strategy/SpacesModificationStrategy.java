public interface SpacesModificationStrategy {
   String modify(String input);
}
