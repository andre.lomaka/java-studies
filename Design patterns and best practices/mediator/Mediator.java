public interface Mediator {
   void sendInfo(Object requester, String context);
}
