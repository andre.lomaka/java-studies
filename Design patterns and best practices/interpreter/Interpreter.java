public interface Interpreter {
   String interpret(String context);
}
