package ee.sda.exercise4;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DummyLogger {

   private void sayHi() {
      log.info("Hi from Exercise4");
   }
}
