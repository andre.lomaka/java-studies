package ee.sda.exercise4;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UtilConfiguration {

   @Bean
   public DummyLogger dummyLogger() {
      return new DummyLogger();
   }

   @Bean
   ListUtil listUtility() {
      return new ListUtil();
   }

   @Bean(name = "stringUtility")
   public StringUtil stringUtil() {
      return new StringUtil();
   }
}
