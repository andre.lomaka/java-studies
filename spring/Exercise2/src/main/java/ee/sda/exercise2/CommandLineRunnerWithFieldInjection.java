package ee.sda.exercise2;

import org.springframework.boot.CommandLineRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(2)
public class CommandLineRunnerWithFieldInjection implements CommandLineRunner {

   @Autowired
   private DummyLogger dummyLogger;

   @Override
   public void run(final String... args) throws Exception {
      dummyLogger.sayHello("CommandLineRunnerWithFieldInjection");
   }
}
