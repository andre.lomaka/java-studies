package ee.sda.exercise2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(3)
public class CommandLineRunnerWithSetterInjection implements CommandLineRunner {

   private DummyLogger dummyLogger;

   @Override
   public void run(final String... args) throws Exception {
      dummyLogger.sayHello("CommandLineRunnerWithSetterInjection");
   }

   @Autowired
   public void setDummyLogger(final DummyLogger dummyLogger) {
      this.dummyLogger = dummyLogger;
   }
}
