package ee.sda.exercise2;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class DummyLogger {
   public void sayHello(String src) {
      log.info("DummyLogger hello from " + src);
   }
}
