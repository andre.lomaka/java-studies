package ee.sda.exercise6;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.*;
import java.util.List;
import java.util.Map;

@Component
@ConfigurationProperties(prefix = "ee.sda.exercise6")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Validated
public class PersonProperties {

   @NotNull(message = "email cannot be null")
   @Email(message = "incorrect email format")
   private String email;

   @NotNull(message = "age is mandatory")
   @Min(18)
   private Integer age;

   private String firstName;

   @NotNull(message = "last name has to be provided")
   @Length(min = 3, max = 20)
   public String lastName;

   private String address;

   @AssertTrue(message = "address is invalid")
   public boolean isAddressValid() {
      return address != null && address.split(" ").length == 2;
   }

   @NotEmpty(message = "values cannot be empty")
   private List<String> values;

   @NotEmpty(message = "custom attributes cannot be empty")
   private Map<String, String> customAttributes;
}
