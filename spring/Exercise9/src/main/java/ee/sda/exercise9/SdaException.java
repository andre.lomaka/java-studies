package ee.sda.exercise9;

public class SdaException extends RuntimeException {
   public SdaException(final String message) {
      super(message);
   }
}
