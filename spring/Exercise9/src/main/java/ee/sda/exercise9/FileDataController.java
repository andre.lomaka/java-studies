package ee.sda.exercise9;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/files-data")
public class FileDataController {

   private final FileDataCrudService fileDataCrudService;

   @GetMapping
   public FileDataCollection getAll() {
      return new FileDataCollection(fileDataCrudService.getAll());
   }

   @PostMapping
   public ResponseEntity<FileData> create(@RequestBody final FileData fileData) throws URISyntaxException {
      URI location = ServletUriComponentsBuilder.
              fromCurrentRequest().
              path("/{id}").
              buildAndExpand(fileDataCrudService.create(fileData).getId()).
              toUri();
      return ResponseEntity.created(location).build();
   }

   @GetMapping("/{id}")
   public FileData getById(@PathVariable final UUID id) {
      return fileDataCrudService.findById(id);
   }

   @PutMapping("/{id}")
   @ResponseStatus(HttpStatus.NO_CONTENT)
   public void update(@PathVariable final UUID id, @RequestBody final FileData fileData) {
      fileDataCrudService.update(id, fileData);
   }

   @DeleteMapping("/{id}")
   @ResponseStatus(HttpStatus.NO_CONTENT)
   public void delete(@PathVariable final UUID id) {
      fileDataCrudService.delete(id);
   }
}
