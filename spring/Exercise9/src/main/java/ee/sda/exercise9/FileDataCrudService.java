package ee.sda.exercise9;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

@Transactional
@Service
@RequiredArgsConstructor
public class FileDataCrudService {

   private final FileDataRepository fileDataRepository;

   public List<FileData> getAll() {
      return new ArrayList<>((Collection<? extends FileData>) fileDataRepository.findAll());
   }

   public FileData create(final FileData fileData) {
      fileData.setId(null);
      return fileDataRepository.save(fileData);
   }

   public FileData findById(final UUID id) {
      return fileDataRepository.findById(id).orElseThrow(() -> new SdaException("No such item"));
   }

   public void update(final UUID id, final FileData fileData) {
      FileData fd = fileDataRepository.findById(id).orElseThrow(() -> new SdaException("No such item"));
      fd.setContent(fileData.getContent());
      fd.setExtension(fileData.getExtension());
      fd.setName(fileData.getName());
      fd.setSizeInKb(fileData.getSizeInKb());
      fileDataRepository.save(fd);
   }

   public void delete(final UUID id) {
      fileDataRepository.findById(id).orElseThrow(() -> new SdaException("No such item"));
      fileDataRepository.deleteById(id);
   }
}
