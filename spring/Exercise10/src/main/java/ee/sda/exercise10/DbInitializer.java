package ee.sda.exercise10;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class DbInitializer implements CommandLineRunner {

   private BookRepository bookRepository;

   public DbInitializer(final BookRepository bookRepository) {
      this.bookRepository = bookRepository;
   }

   @Override
   public void run(final String... args) throws Exception {
      List<Book> books = bookRepository.findAllByTitle("T1");
      books.forEach(System.out::println);
      System.out.println();

      Optional<Book> bookOpt = bookRepository.findFirstByISBN("I3");
      if (bookOpt.isPresent()) System.out.println(bookOpt.get());
      else System.out.println("No such ISBN");
      System.out.println();

      books = bookRepository.findAllByTitleAndAuthor("T1", "A2");
      if (books.isEmpty()) System.out.println("No such book");
      books.forEach(System.out::println);
      System.out.println();

      books = bookRepository.findTop3ByAuthorOrderByPagesNumDesc("A1");
      books.forEach(System.out::println);
      System.out.println();

      books = bookRepository.findAllByTitleStartingWith("T");
      books.forEach(System.out::println);
      System.out.println();

      books = bookRepository.findAllByPagesNumBetween(100, 200);
      books.forEach(System.out::println);
      System.out.println();

      books = bookRepository.findWherePagesNumIsGreaterThanX(200);
      books.forEach(System.out::println);
      System.out.println();
   }
}
