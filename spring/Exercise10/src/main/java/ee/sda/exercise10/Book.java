package ee.sda.exercise10;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "books")
public class Book {
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Long id;

   @Column(name = "title")
   private String title;

   @Column(name = "author")
   private String author;

   @Column(name = "ISBN")
   private String ISBN;

   @Column(name = "pages_num")
   private Integer pagesNum;
}
