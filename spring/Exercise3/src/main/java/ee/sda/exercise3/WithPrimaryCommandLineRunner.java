package ee.sda.exercise3;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class WithPrimaryCommandLineRunner implements CommandLineRunner {

   private final DummyLogger dummyLogger;

   public WithPrimaryCommandLineRunner(final DummyLogger dummyLogger) {
      this.dummyLogger = dummyLogger;
   }

   @Override
   public void run(final String... args) throws Exception {
      dummyLogger.sayHello();
   }
}
