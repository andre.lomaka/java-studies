package ee.sda.exercise3;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class SecondDummyLogger implements DummyLogger {

   @Override
   public void sayHello() {
      log.info("Hello from SecondDummyLogger");
   }
}
