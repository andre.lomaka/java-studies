package ee.sda.exercise8;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class RandomNumberController {

   private final RandomBooleanProvider randomNumberProvider;

   @GetMapping("/api/random-boolean")
   public Boolean showFirstNumber() {
      return randomNumberProvider.getValue();
   }

}
