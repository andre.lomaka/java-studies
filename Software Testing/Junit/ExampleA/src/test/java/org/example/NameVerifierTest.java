package org.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class NameVerifierTest {

   @Test
   void shouldValidateName() {
      String name = "Adam";
      NameVerifier nameVerifier = new NameVerifier();

      final boolean validationResult = nameVerifier.isNameValid(name);

      assertTrue(validationResult);
   }

   @Test
   void shouldNotValidateNameWhenAllLettersAreLowerCase() {
      String name = "adam";
      NameVerifier nameVerifier = new NameVerifier();

      final boolean validationResult = nameVerifier.isNameValid(name);

      assertFalse(validationResult);
   }

   @Test
   void shouldNotValidateEmptyName() {
      String name = "";
      NameVerifier nameVerifier = new NameVerifier();

      final boolean validationResult = nameVerifier.isNameValid(name);

      assertFalse(validationResult);
   }

   @Test
   void shouldNotValidateNullName() {
      String name = null;
      NameVerifier nameVerifier = new NameVerifier();

      final boolean validationResult = nameVerifier.isNameValid(name);

      assertFalse(validationResult);
   }
}
