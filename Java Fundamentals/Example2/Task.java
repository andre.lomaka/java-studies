import java.util.Scanner;
import java.util.Arrays;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.util.function.Function;

public class Task {
   public static void main(String[] args) {
      Scanner scanner = new Scanner(System.in);
      String text = scanner.nextLine();
      String lowerCaseText = text.toLowerCase();

      String[] words =lowerCaseText.split("[^a-z]+");
      System.out.println("Words sorted with Arrays.sort():");
      Arrays.sort(words);
      String lastWord = "";
      int count = 0;
      for (int i = 0; i < words.length; i++) {
         String currentWord = words[i];
         if (i == 0) {
            count++;
            lastWord = currentWord;
            continue;
         }
         if (currentWord.equals(lastWord)) {
            count++;
         } else {
            System.out.println(lastWord + " - " + count);
            lastWord = currentWord;
            count = 1;
         }
         if (i == words.length-1) {
            System.out.println(lastWord + " - " + count);
         }
      }

      words = lowerCaseText.split("[^a-z]+");
      System.out.println();
      System.out.println("Words sorted with Stream:");
      Stream.of(words)
//               .sorted()
//               .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
               .collect(Collectors.groupingBy(e -> e, Collectors.counting()))
               .entrySet()
               .stream()
               .sorted((a,b) -> a.getKey().compareTo(b.getKey()))
               .forEach(e -> System.out.printf("%s - %d%n", e.getKey(), e.getValue()));
   }
}
