public class Task {
   public static void main(String[] args) {
      int i, j;
      String outstr;
      for (i=1; i <= 10; i++) {
         for (j=1; j <= 10; j++) {
            outstr = String.format("%4s", i*j);
            System.out.print(outstr);
         }
         System.out.println();
      }
   }
}
